# Setup and play with this project


## Clone and Install

    git clone https://gitlab.com/shivgupt/communitydao.git
    cd CommunityDAO
    npm i

## Start subgraph and ganache
    cd subgraph
    npm i
    npm run docker:run
    

## Start another tab (got to main project folder) and create `.env` file in main directory

    NETWORK='private'
    PROVIDER='http://localhost:8545'

    # This is ganache deterministic private key ... not mine 😝
    PRIVATE_KEY='0x4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d'
    OUTPUT_FILE='ops/migration.json'
    CUSTOM_ABI_LOCATION='build/contracts'

    npm run migrate

## Deploy subgraph
    npm run deploy '{  "migrationFile" : "../../ops/migration.json" }'

## Interact with contracts

    node test.js
