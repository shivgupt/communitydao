const ethers = require('ethers');

const provider = new ethers.providers.JsonRpcProvider();
const privateKey = '0x6cbed15c793ce57650b9877cf6fa156fbef513c4e6134f022a85b1ffdd59b2a1'
const wallet = new ethers.Wallet(privateKey, provider)
const migration = require('./ops/migration.json')
  
//console.log(migration)
const avatar = migration.private.dao['0.0.1-rc.24'].Avatar
const buyInWithRageQuitOptAddress = migration.private.dao['0.0.1-rc.24'].Schemes[0].address
const controllerAddress = migration.private.dao['0.0.1-rc.24'].Controller
const votingMachineAddress = migration.private.base['0.0.1-rc.24'].GenesisProtocol

const BuyInWithRageQuitOptAbi = require("./build/contracts/BuyInWithRageQuitOpt.json").abi
const BuyInWithRageQuitOpt = new ethers.Contract(buyInWithRageQuitOptAddress, BuyInWithRageQuitOptAbi, provider);
const BuyInWithRageQuitOptWSigner = BuyInWithRageQuitOpt.connect(wallet);

const ControllerAbi = require("@daostack/arc/build/contracts/Controller.json").abi
const Controller = new ethers.Contract(controllerAddress, ControllerAbi, provider);
const ControllerWSigner = BuyInWithRageQuitOpt.connect(wallet);

const VotingMachineAbi = require("./build/contracts/GenesisProtocol.json").abi
const VotingMachine = new ethers.Contract(votingMachineAddress, VotingMachineAbi, provider);
const VotingMachineWSigner = VotingMachine.connect(wallet);


const getReputation = () => {
  BuyInWithRageQuitOpt.reputation().then((rep) => { console.log(rep) })
}

const deposit = (amt) => {
  BuyInWithRageQuitOptWSigner.deposit({
    gasLimit: 7300000,
    value: ethers.utils.bigNumberify(amt)
  }).then( tx => {
    tx.wait().then(res => {
      console.log("Deposit Result: ", JSON.stringify(res, null, 2))
    })
  })
}

const quit = () => {
  BuyInWithRageQuitOptWSigner.quit({ gasLimit: 4300000}).then(tx => {
    tx.wait().then(res => {
      console.log("Quit Result: ", JSON.stringify(res, null, 2))
    })
  })
}

const getSchemes = () => {
  Controller.schemes(buyInWithRageQuitOptAddress).then(rep => {
    console.log("Scheme params & permissions: ", JSON.stringify(rep, null, 2))
  })
}

const getAvatar = () => {
  Controller.avatar().then(rep => {
    console.log("Avatar: ", JSON.stringify(rep, null, 2))
  })
}

const getReputationFromController = () => {
  Controller.nativeReputation().then(rep => {
    console.log("Reputation: ", JSON.stringify(rep, null, 2))
  })
}

//getAvatar()
//getReputationFromController()
//getSchemes()
deposit('10000000000000000000')
//quit()

provider.getBalance(avatar).then((balance) => {
    let etherString = ethers.utils.formatEther(balance);
    console.log("Balance: " + etherString);
});
