import 'allocator/arena';

import {
  store,
} from '@graphprotocol/graph-ts';

import * as domain from '../../domain';

import {
  Deposit,
  Quit
} from '../../types/schema';

import { concat, equalsBytes, eventId } from '../../utils';

import {
  buyIn,
  rageQuit,
} from '../../types/BuyInWithRageQuitOpt/BuyInWithRageQuitOpt';

export function handleBuyIn(event: buyIn): void {
  let ent = new Deposit(eventId(event));
  ent.memberAddress = event.params._member;
  ent.amount = event.params._amount;
  ent.rep = event.params._rep;

  store.set('Deposit', ent.id, ent);
}

export function handleRageQuit(event: rageQuit): void {
  let ent = new Quit(eventId(event));
  ent.memberAddress = event.params._member;
  ent.amount = event.params._amount;
  ent.rep = event.params._rep;

  store.set('Quit', ent.id, ent);
}
